;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of 
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     yaml
     html
     vimscript
     javascript
     evil-snipe   ;; RBC: 20180626 allows jump to next 2 characters
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     helm

     ;; RBC 20181015: prevent ENTER from completing and enable some options (taken from official doc)
     (auto-completion :variables
                      auto-completion-return-key-behavior nil
                      auto-completion-enable-snippets-in-popup t
                      auto-completion-enable-help-tooltip t
                      auto-completion-enable-sort-by-usage t
                      )

     ;; better-defaults
     emacs-lisp
     git
     markdown
     ;; RBC 20180906: activate github
     github
     ;; RBC 20180711: activate org with custom config
     ( org :variables
           org-enable-reveal-js-support t
           org-projectile-file "TODO.org"
           ;;org-bullets-bullet-list '("■" "◆" "▲" "▶")
           ;;org- No recursion...
           org-agenda-files (directory-files-recursively "~/org/" "\.org$")         ;; this forces search in every org files anywhere in ~/org subdirectories 
                                                                                    ;; CAVEAT: note though that if a file is added, it won't be seen by search until config is reloaded 
           org-log-into-drawer t
           org-clock-into-drawer "TRACKER"
           org-extend-today-until 4                                 ;; try to modify the end of day hour to 4:00 (for clocktables). But it doesn't seem to work...
     )
     ;; (shell :variables
     ;;        shell-default-height 30
     ;;        shell-default-position 'bottom)
     ;; version-control
     w3m ;; RBC 20180628: enabled w3m browser integration (See https://github.com/venmos/w3m-layer )

     ;; RBC 20180115: activate php and python 
     php
     python

     ;; RBC 20181027: activate spell-checking and syntax-checking (with flycheck)
     ;; spell shecking by default is really annoying!
     ( spell-checking :variables
            spell-checking-enable-by-default nil
     )
   )

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   ;; RBC 20180625: Added color-theme-solarize
   ;; RBC 20180916: Added deft & helm-org-rifle (with it's dependencies: dash, f and s)  [TODO] remove dash/s/f/helm-org-rifle.
   ;; RBC 20180918: Added shell-pop
   ;; RBC 20180919: Added org-autolist
   ;; RBC 20180921: Added git-timemachine
   ;; RBC 20181015: Added php-extras
   ;; dotspacemacs-additional-packages '(color-theme-solarized org-super-agenda shell-pop org-autolist git-timemachine php-extras)
      dotspacemacs-additional-packages '(color-theme-solarized org-super-agenda dash f s helm-org-rifle shell-pop org-autolist git-timemachine php-extras)

   ;; A list of packages that cannot be updated.

   dotspacemacs-excluded-packages '(evil-search-highlight-persist  org-bullets evil-escape)
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'."
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))
   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light
                         )
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Source Code Pro"
                               :size 13
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ nil
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts t  ;; RBC 20180627 want to reload layout automatically
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always

   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   ;; dotspacemacs-enable-paste-transient-state t   ;; RBC 20180626. Activate multi-level kill ring. (p: paste first ring, p p : paste second, ...)

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols nil 
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers t

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil
   ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
   It is called immediately after `dotspacemacs/init', before layer configuration executes.
   This function is mostly useful for variables that need to be set before packages are loaded.
   If you are unsure, you should try in setting them in `dotspacemacs/user-config' first."
)

(defun dotspacemacs/user-config ()

  "Configuration function for user code.
  This function is called at the very end of Spacemacs initialization after
  layers configuration.
  This is the place where most of your configurations should be done. Unless it is
  explicitly specified that a variable should be set before a package is loaded,
  you should place your code here."

  ;; RBC 201807208: custom line number
  ;; [BUG] le nouveau backend 'display-line-number-mode' plus rapide que l'ancien 'linum-mode' fait bugger linum-relative-format (voir linum-relative.el)
  ;; là ça marchouille mais je sais pas trop comment...
  (linum-relative-mode)
  (setq linum-relative-format "%3s \u2502 ")
  (setq linum-format "%4d \u2502 ")  
  (set-face-foreground 'linum "#aaa")

  ;; RBC 20180625: set paradox package token to allow github access
  (setq paradox-github-token "c18f5df5a922e6f994cffbef040108dd23debd4a")
  ;; RBC 20180625: configure solarized version to dark in the terminal (https://github.com/sellout/emacs-color-theme-solarized)
  (set-terminal-parameter nil 'background-mode 'dark)
  ;; RBC 20180625: get ride of backgound comment in solarized theme
  (setq spacemacs-theme-comment-bg nil)

  ;; RBC 20180628: workaround "no usable browser found" error (cf https://stackoverflow.com/questions/25261200/no-usable-browser-found-error-when-using-emacs-to-browse-hyperspec)
  (setq browse-url-generic-program (executable-find "w3m")
        browse-url-browser-function 'browse-url-generic)


  ;; RBC 20180703: no highlighting of current line
  (global-hl-line-mode -1)

  ;; RBC 20180711: avoid huge TAG files
  (with-eval-after-load 'helm-projectile
    (add-to-list 'projectile-globally-ignored-directories "node_modules")
  )

  ;; RBC 20180711: org configuration
  (with-eval-after-load 'org

    (setq org-capture-templates '(("i" "TODO [inbox]" entry
                                   (file+headline "~/org/gtd/inbox.org" "Tasks")
                                   "* %i%?")
                                  ("T" "TICKLER" entry
                                   (file+headline "~/org/gtd/tickler.org" "Tickler")
                                   "* %i%? \n %U")
                                  ("c" "CHAT" entry
                                   (file+headline "~/org/gtd/tickler.org" "Meeting")
                                   "* MEETING %i%? \n %U" :clock-in t)
                                  ("p" "PAUSE" entry (file+datetree "~/org/gtd/diary.org")
                                     "* %?\n%U\n" :clock-in t :clock-resume t)
                                  ))

    (setq org-refile-targets '(("~/org/gtd/todo.org" :maxlevel . 1)
                               ("~/org/gtd/someday.org" :level . 1)
                               ("~/org/gtd/tickler.org" :maxlevel . 1)))

    (setq org-refile-use-outline-path 'file)                 ;; enable to refile header at point to toplevel in target file
                                                             ;; sourced [[https://emacs.stackexchange.com/a/13362][here]]

    ;; RBC 230180904: enable indent mode in org and open all fold


    ;; RBC 20180806. Testing Agenda Customization
     (setq org-agenda-custom-commands
          '(("g" "GTD Agenda View"
             ((agenda ""
                    ((org-agenda-span 1)
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                     (org-agenda-overriding-header "AGENDA")))
              (todo ""
                    ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "NEXT" "SOMEDAY" "DONE" "CANCELED" )))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                     (org-agenda-overriding-header "WIP")))
              (todo ""
                       ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "WAIT" "RUN" "SOMEDAY" "DONE" "CANCELED" )))
                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                        (org-agenda-overriding-header "Next")))
              (todo ""
                    ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("NEXT" "RUN" "WAIT" "SOMEDAY" "DONE" "CANCELED" )))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                     (org-agenda-overriding-header "Backlog")))
              (stuck ""
                     ((org-agenda-overriding-header "Stucks")))
              ))
            ("G" "GTD View"
              ((todo ""
                     ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "NEXT" "SOMEDAY" "DONE" "CANCELED" )))
                      (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                      (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                      (org-agenda-overriding-header "WIP")))
               (todo ""
                     ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "WAIT" "RUN" "SOMEDAY" "DONE" "CANCELED" )))
                      (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                      (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                      (org-agenda-overriding-header "Next")))
              (todo ""
                    ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("NEXT" "RUN" "WAIT" "SOMEDAY" "DONE" "CANCELED" )))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
                     (org-agenda-overriding-header "Backlog")))
              (stuck ""
                     ((org-agenda-overriding-header "Stucks")))
             ))))

    ;; (setq org-agenda-custom-commands
    ;;       '(("G" "GTD View"
    ;;          ((todo ""
    ;;                 ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "NEXT" "SOMEDAY" "DONE" "CANCELED" )))
    ;;                  (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
    ;;                  (org-agenda-overriding-header "WIP")))

    ;;           (todo ""
    ;;                    ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("TODO" "WAIT" "RUN" "SOMEDAY" "DONE" "CANCELED" )))
    ;;                     (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
    ;;                     (org-agenda-overriding-header "Next")))
    ;;           (todo ""
    ;;                 ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("NEXT" "RUN" "WAIT" "SOMEDAY" "DONE" "CANCELED" )))
    ;;                  (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:drill\:"))
    ;;                  (org-agenda-skip-function '(org-agenda-skip-entry-if 'regexp "\:habit\:"))
    ;;                  (org-agenda-overriding-header "Backlog")))
    ;;           (stuck ""
    ;;                  ((org-agenda-overriding-header "Stucks")))
    ;;          ))))

    ;; RBC 20180905: configuring org-drill
    ;; "By default, you will be prompted to save all unsaved buffers at the end of a drill session. If you don't like this behaviour, use the following setting:"
    (setq org-drill-save-buffers-after-drill-sessions-p nil)
    (setq org-drill-maximum-items-per-session 90)
    (setq org-drill-maximum-duration 60)   ; 60 minutes 
  )

  ;; RBC 20180916: deft configuration
  ;; deft is an emacs based notes management utility 
  ;;  (with-eval-after-load 'deft
  ;;     (setq deft-default-extension "org")
  ;;     (setq deft-extensions '("org" "md"))
  ;;     (setq deft-directory "~/org/notes/deft/")
  ;;     ;;(setq deft-text-mode 'org-mode)
  ;;     (setq deft-recursive t)
  ;;     (setq deft-use-filter-string-for-filename t)
  ;;     (setq deft-file-naming-rules
  ;;           '((noslash . "-")
  ;;             (nospace . "-")
  ;;             (case-fn . downcase)))
  ;;     ;; [TODO] créer un issue sur github pour demander le contrôle sur le summary (Comment obliger Deft à arrêter le summary sur une fin de ligne?)
  ;;     (setq deft-strip-summary-regexp "^#\\+[[:upper:]_]+:.*$")
  ;;  )

  ;; RBC 20180916: helm-org-rifle configuration
  ;; If we wait for rifle loading, key bindings are not set until we manually `SPC SPC helm-org-rifle' 
  ;;  (with-eval-after-load 'helm-org-rifle
    (message "RBC: rifle loading")
    (setq helm-org-rifle-show-path nil)
    ;; define key bindings (they'll all be behind ", r") 
    (defun rifle-snippets () "use helm-org-rifle to drill through snippets" (interactive) (helm-org-rifle-directories "~/org/snippets/")) 
    (define-key evil-normal-state-map (kbd ", r s") 'rifle-snippets)

    (defun rifle-org () "use helm-org-rifle to drill through org directory" (interactive) (helm-org-rifle-directories "~/org/"))
    (define-key evil-normal-state-map (kbd ", r o") 'rifle-org)

    (defun rifle-agenda () "use helm-org-rifle to drill through agenda" (interactive) (helm-org-rifle-directories "~/org/gtd"))
    (define-key evil-normal-state-map (kbd ", r a") 'rifle-agenda)

    (defun rifle-notes () "use helm-org-rifle to drill through notes" (interactive) (helm-org-rifle-directories "~/org/notes"))
    (define-key evil-normal-state-map (kbd ", r n") 'rifle-notes)

  ;; )

  ;; RBC 20180918: shell-pop configuration
  ;; If we wait for rifle loading, key bindings are not set until we manually `SPC SPC shell-pop' 
;;  (with-eval-after-load 'shell-pop
    (setq shell-pop-shell-type (quote ("ansi-term" "*ansi-term*" (lambda nil (ansi-term shell-pop-term-shell) ))))
    (setq shell-pop-term-shell "/bin/bash")
    ;; need to do this manually or not picked up by `shell-pop'
    ;;(shell-pop--set-shell-type 'shell-pop-shell-type shell-pop-shell-type)
    (define-key evil-normal-state-map (kbd ", o x") 'shell-pop)
;; )

  ;; RBC 20180919: org-autolist configuration (allows smarter ENTER in lists)
  (with-eval-after-load 'org-autolist
    (add-hook 'org-mode-hook (lambda () (org-autolist-mode)))
  )

  ;; RBC 20181019: declare active languages in org-babel
  ;; active Babel languages
  ;; [TODO] bash is recognized by default ?
  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (python . t)
     (shell . t)
     (js t)
    ))

  ;; RBC 20190105: Send notification to [[https://mashlol.github.io/notify/][Open Source Notify Android App]]
  ;; [[https://github.com/mashlol/notify][notify-cli npm package]] must be installed globally and configured with notify key with 'notify -r <KEY>'
  (with-eval-after-load 'org-pomodoro

    (add-hook 'org-pomodoro-started-hook
              (lambda ()
                "Hooks run when a pomodoro is started."
                (interactive)
                (message "RBC: pomodoro start")
                (start-process-shell-command "pomodoro-alert" (get-buffer-create "*notify*") "gotify push \"pomodoro start\" ")))

    (add-hook 'org-pomodoro-finished-hook
              (lambda ()
                "Hooks run when a pomodoro is finished."
                (interactive)
                (message "RBC: pomodoro finish")
                (start-process-shell-command "pomodoro-alert-finish" (get-buffer-create "*notify*") "gotify push \"pomodoro finish\" ")))

    (add-hook 'org-pomodoro-killed-hook
              (lambda ()
                "Hooks run when a pomodoro is killed."
                (interactive)
                (message "RBC: pomodoro killed")
                (start-process-shell-command "pomodoro-alert-killed" (get-buffer-create "*notify*") "gotify push \"pomodoro killed\" ")))

    (add-hook 'org-pomodoro-break-finished-hook 
              (lambda ()
                "Hook run after any break has finished."
                (interactive)
                (message "RBC: pomodoro break finished")
                (start-process-shell-command "pomodoro-alert-break-finished" (get-buffer-create "*notify*") "gotify push \"pomodoro break finished\" ")))

    (setq org-pomodoro-length 90)
    (setq org-pomodoro-short-break-length 30)
    (setq org-pomodoro-long-break-length 60)
  )
;; delete me
(defun pop-global-mark ()
  "Pop off global mark ring and jump to the top location."
  (interactive)
  ;; Pop entries which refer to non-existent buffers.
  (message "RBC pop global mark")
  (while (and global-mark-ring (not (marker-buffer (car global-mark-ring))))
    (setq global-mark-ring (cdr global-mark-ring)))
  (message (nconc '("STEP 1: ") global-mark-ring))
  (or global-mark-ring
      (error "No global mark set"))
  (message (nconc '("STEP 2: ") global-mark-ring))
  (let* ((marker (car global-mark-ring))
          (buffer (marker-buffer marker))
          (position (marker-position marker)))

    (message (nconc '("marker: ") marker))
    (message (nconc '("buffer: ") buffer))
    (message (nconc '("position: ") position))

    (setq global-mark-ring (nconc (cdr global-mark-ring)
                                  (list (car global-mark-ring))))

    (set-buffer buffer)
    (message (nconc '("STEP 3: ") global-mark-ring))
    (message (nconc '("marker: ") marker))
    (message (nconc '("buffer: ") buffer))
    (message (nconc '("position: ") position))

    (or (and (>= position (point-min))
              (<= position (point-max)))
        (if widen-automatically
            (widen)

    (goto-char position)
    (switch-to-buffer buffer)))))

 (setq epa-pinentry-mode 'loopback)


;; RBC 20180711: Run this code after js-mode has been loaded. Otherwise it generates a error at startup
  (with-eval-after-load 'js2-mode
    ;; RBC 20180703: prevent js2 mode from warning on missing semicolons
    (setq js2-missing-semi-one-line-override t )
    (setq js2-mode-show-strict-warnings nil)
    (setq js2-strict-missing-semi-warning nil )
    (message "RBC: js-mode configured")
  )

;; RBC correct helm highlight color schem as was unreadable 
(set-face-attribute 'helm-selection nil
                    :background "purple"
                    :foreground "black")

;; Indent to column 100 (Doesn't work for now. =todo= make it work)
(define-key evil-normal-state-map (kbd "TAB") (lambda () (indent-to 100)))

)

;; DO not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#bcbcbc" "#d70008" "#5faf00" "#875f00" "#268bd2" "#800080" "#008080" "#5f5f87"])
 '(avy-background nil t)
 '(evil-want-Y-yank-to-eol nil)
 '(frame-background-mode (quote dark))
 '(linum-relative-format "%3s  ")
 '(neo-theme (quote nerd) nil nil "RBC 20182606 (change neotree theme to display arrows)")
 '(org-agenda-files
   (quote
    ("/home/rbc/org/cheat/css.org" "/home/rbc/org/cheat/emmet.org" "/home/rbc/org/cheat/phonetic-alphabet.org" "/home/rbc/org/cheat/semver.org" "/home/rbc/org/cheat/unicode.org" "/home/rbc/org/gtd/diary.org" "/home/rbc/org/gtd/doing.org" "/home/rbc/org/gtd/habits.org" "/home/rbc/org/gtd/inbox.org" "/home/rbc/org/gtd/journal.org" "/home/rbc/org/gtd/someday.org" "/home/rbc/org/gtd/tickler.org" "/home/rbc/org/gtd/todo.org" "/home/rbc/org/notes/books/reading/4-hour-chef.org" "/home/rbc/org/notes/books/toread/nobody-wants-to-read-your-shit.org" "/home/rbc/org/notes/create/ideas/photos.org" "/home/rbc/org/notes/create/ideas.org" "/home/rbc/org/notes/create/quotes.org" "/home/rbc/org/notes/drafts/hyperledger.org" "/home/rbc/org/notes/general/Tunisia-Economy.org" "/home/rbc/org/notes/general/africa.org" "/home/rbc/org/notes/general/funds.org" "/home/rbc/org/notes/general/gbo.org" "/home/rbc/org/notes/general/sales.org" "/home/rbc/org/notes/general/smartcity.org" "/home/rbc/org/notes/general/startup-tunisia.org" "/home/rbc/org/notes/howto/aria2.org" "/home/rbc/org/notes/howto/aws.org" "/home/rbc/org/notes/howto/backup.org" "/home/rbc/org/notes/howto/bash-redirection.org" "/home/rbc/org/notes/howto/bash.org" "/home/rbc/org/notes/howto/blockchain.org" "/home/rbc/org/notes/howto/book.org" "/home/rbc/org/notes/howto/browser.org" "/home/rbc/org/notes/howto/caddy.org" "/home/rbc/org/notes/howto/cli.org" "/home/rbc/org/notes/howto/cloudstation.org" "/home/rbc/org/notes/howto/colors.org" "/home/rbc/org/notes/howto/container.org" "/home/rbc/org/notes/howto/css.org" "/home/rbc/org/notes/howto/devtools.org" "/home/rbc/org/notes/howto/diagrams.org" "/home/rbc/org/notes/howto/dns.org" "/home/rbc/org/notes/howto/docker.org" "/home/rbc/org/notes/howto/elisp.org" "/home/rbc/org/notes/howto/email.org" "/home/rbc/org/notes/howto/encoding.org" "/home/rbc/org/notes/howto/expect.org" "/home/rbc/org/notes/howto/fasd.org" "/home/rbc/org/notes/howto/firebase.org" "/home/rbc/org/notes/howto/ftp.org" "/home/rbc/org/notes/howto/fzf.org" "/home/rbc/org/notes/howto/git.org" "/home/rbc/org/notes/howto/incorporation.org" "/home/rbc/org/notes/howto/iptables.org" "/home/rbc/org/notes/howto/javascript.org" "/home/rbc/org/notes/howto/jquery.org" "/home/rbc/org/notes/howto/lifehacks.org" "/home/rbc/org/notes/howto/magit.org" "/home/rbc/org/notes/howto/makefile.org" "/home/rbc/org/notes/howto/mosh.org" "/home/rbc/org/notes/howto/network.org" "/home/rbc/org/notes/howto/nodejs.org" "/home/rbc/org/notes/howto/oauth2.org" "/home/rbc/org/notes/howto/orchestrators.org" "/home/rbc/org/notes/howto/php.org" "/home/rbc/org/notes/howto/postcss.org" "/home/rbc/org/notes/howto/python-vs-javascript.org" "/home/rbc/org/notes/howto/python.org" "/home/rbc/org/notes/howto/sales.org" "/home/rbc/org/notes/howto/sandboxcloud.org" "/home/rbc/org/notes/howto/security.org" "/home/rbc/org/notes/howto/spaced-learning.org" "/home/rbc/org/notes/howto/spacemacs.org" "/home/rbc/org/notes/howto/speedreading.org" "/home/rbc/org/notes/howto/ssh.org" "/home/rbc/org/notes/howto/staticweb.org" "/home/rbc/org/notes/howto/tailwindcss.org" "/home/rbc/org/notes/howto/tests-javascript.org" "/home/rbc/org/notes/howto/tmux.org" "/home/rbc/org/notes/howto/vscode.org" "/home/rbc/org/notes/howto/webperf.org" "/home/rbc/org/notes/howto/webserver.org" "/home/rbc/org/notes/howto/workstation.org" "/home/rbc/org/notes/learn/learning.org" "/home/rbc/org/notes/lists/books.org" "/home/rbc/org/notes/lists/domains.org" "/home/rbc/org/notes/lists/idioms.org" "/home/rbc/org/notes/lists/mailinglists.org" "/home/rbc/org/notes/lists/music.org" "/home/rbc/org/notes/lists/quotes.org" "/home/rbc/org/notes/lists/tools.org" "/home/rbc/org/notes/lists/whoswho.org" "/home/rbc/org/notes/sos.org" "/home/rbc/org/now/dump.org" "/home/rbc/org/projects/africafrance.org" "/home/rbc/org/projects/biowaste.org" "/home/rbc/org/projects/expert.org" "/home/rbc/org/references/org-mode.org" "/home/rbc/org/snippets/README.org" "/home/rbc/org/snippets/apt.org" "/home/rbc/org/snippets/chrome.org" "/home/rbc/org/snippets/console.org" "/home/rbc/org/snippets/css-bootstrap.org" "/home/rbc/org/snippets/css.org" "/home/rbc/org/snippets/disk.org" "/home/rbc/org/snippets/figma.org" "/home/rbc/org/snippets/git.org" "/home/rbc/org/snippets/grep.org" "/home/rbc/org/snippets/html.org" "/home/rbc/org/snippets/ip.org" "/home/rbc/org/snippets/javascript.org" "/home/rbc/org/snippets/linux.org" "/home/rbc/org/snippets/live-webserver.org" "/home/rbc/org/snippets/makefile.org" "/home/rbc/org/snippets/osint.org" "/home/rbc/org/snippets/php.org" "/home/rbc/org/snippets/python.org" "/home/rbc/org/snippets/rsync.org" "/home/rbc/org/snippets/scaffolding.org" "/home/rbc/org/snippets/ssh.org" "/home/rbc/org/snippets/systemd.org" "/home/rbc/org/snippets/termux.org" "/home/rbc/org/snippets/time.org" "/home/rbc/org/snippets/tmux.org" "/home/rbc/org/snippets/torrent.org" "/home/rbc/org/snippets/transfer.org" "/home/rbc/org/snippets/watch.org" "/home/rbc/org/snippets/windows.org" "/home/rbc/org/snippets/yaml.org" "/home/rbc/org/srs/memo-css.org" "/home/rbc/org/srs/memo-general.org" "/home/rbc/org/srs/memo-javascript.org" "/home/rbc/org/srs/memo-python.org")))
 '(org-link-frame-setup
   (quote
    ((vm . vm-visit-folder-other-frame)
     (vm-imap . vm-visit-imap-folder-other-frame)
     (gnus . org-gnus-no-new-news)
     (file . find-file)
     (wl . wl-other-frame))))
 '(org-modules
   (quote
    (org-bbdb org-bibtex org-docview org-gnus org-habit org-info org-irc org-mhe org-rmail org-w3m org-drill)))
 '(package-selected-packages
   (quote
    (docker-tramp yaml-mode flyspell-correct-helm flyspell-correct flycheck-pos-tip flycheck auto-dictionary yapfify pyvenv pytest pyenv-mode py-isort pip-requirements live-py-mode hy-mode helm-pydoc cython-mode company-anaconda anaconda-mode pythonic company-quickhelp pos-tip phpunit phpcbf php-extras php-auto-yasnippets drupal-mode php-mode org-autolist shell-pop helm-org-rifle deft magit-gh-pulls github-search github-clone gist gh marshal logito github-browse-file pcache org-super-agenda ht ox-reveal org-projectile org-category-capture org-present org-pomodoro alert log4e gntp org-mime org-download mmm-mode markdown-toc markdown-mode htmlize gnuplot gh-md web-mode tagedit slim-mode scss-mode sass-mode pug-mode less-css-mode helm-css-scss haml-mode emmet-mode company-web web-completion-data helm-w3m w3m helm-company helm-c-yasnippet fuzzy company-tern dash-functional company-statistics company auto-yasnippet ac-ispell auto-complete vimrc-mode dactyl-mode evil-snipe color-theme-solarized color-theme tern smeargle orgit magit-gitflow helm-gitignore gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link evil-magit magit magit-popup git-commit ghub let-alist with-editor web-beautify livid-mode skewer-mode simple-httpd json-mode json-snatcher json-reformat js2-refactor yasnippet multiple-cursors js2-mode js-doc coffee-mode ws-butler winum which-key volatile-highlights vi-tilde-fringe uuidgen use-package toc-org spaceline restart-emacs request rainbow-delimiters popwin persp-mode pcre2el paradox org-plus-contrib open-junk-file neotree move-text macrostep lorem-ipsum linum-relative link-hint indent-guide hungry-delete hl-todo highlight-parentheses highlight-numbers highlight-indentation helm-themes helm-swoop helm-projectile helm-mode-manager helm-make helm-flx helm-descbinds helm-ag google-translate golden-ratio flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu elisp-slime-nav dumb-jump diminish define-word column-enforce-mode clean-aindent-mode auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line)))
 '(read-quoted-char-radix 16)
 '(safe-local-variable-values (quote ((org-bullets-mode))))
 '(ses-after-entry-functions (quote (next-line)))
 '(spacemacs-theme-comment-bg nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(avy-lead-face ((t (:background "magenta" :foreground "black"))))
 '(avy-lead-face-0 ((t (:background "green" :foreground "black"))))
 '(avy-lead-face-1 ((t (:background "gray" :foreground "black"))))
 '(avy-lead-face-2 ((t (:background "#f86bf3" :foreground "black"))))
 '(org-hide ((t (:foreground "black")))))
